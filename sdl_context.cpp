#include "sdl_context.hpp"

SDLContext::SDLContext(int major, int minor)
{
    SDL_Init(SDL_INIT_VIDEO);

    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK,
                        SDL_GL_CONTEXT_PROFILE_CORE);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, major);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, minor);
    SDL_GL_SetAttribute(SDL_GL_STENCIL_SIZE, 8);

    window_ =
        SDL_CreateWindow("OpenGL", SDL_WINDOWPOS_CENTERED,
                         SDL_WINDOWPOS_CENTERED, 800, 600, SDL_WINDOW_OPENGL);

    gl_context_ = SDL_GL_CreateContext(window_);
}

SDLContext::~SDLContext()
{
    SDL_GL_DeleteContext(gl_context_);

    SDL_Quit();
}

void SDLContext::handle_events(const std::function<void(SDL_Event)> &handler)
{
    event_loop_running_ = true;
    SDL_Event window_event;
    while (event_loop_running_) {
        if (SDL_PollEvent(&window_event)) {
            handler(window_event);
        }
        SDL_GL_SwapWindow(window_);
    }
}

void SDLContext::stop_loop()
{
    event_loop_running_ = false;
}
