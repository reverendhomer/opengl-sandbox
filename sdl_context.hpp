#pragma once

#include <SDL.h>
#include <functional>

class SDLContext
{
public:
    SDLContext(int major, int minor);
    ~SDLContext();

    inline SDL_Window *window() noexcept { return window_; }
    inline SDL_GLContext gl_context() const noexcept { return gl_context_; }

    void handle_events(const std::function<void(SDL_Event)> &handler);
    void stop_loop();

private:
    SDL_Window *window_ = nullptr;
    SDL_GLContext gl_context_;
    bool event_loop_running_;
};
