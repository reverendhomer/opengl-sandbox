#include <iostream>

#include "sdl_context.hpp"

#include <GL/glew.h>
#include <SDL.h>

const char *vertexSource =
#include "main.vert.h"
    ;

const char *fragmentSource =
#include "main.frag.h"
    ;

void handle_sdl_event(SDLContext &ctx, SDL_Event event)
{
    if (event.type == SDL_QUIT ||
        (event.type == SDL_KEYUP && event.key.keysym.sym == SDLK_ESCAPE)) {
        ctx.stop_loop();
    }
}

void check_shader_compilation_status(GLuint shader)
{
    GLint status;
    glGetShaderiv(shader, GL_COMPILE_STATUS, &status);

    char buf[512];
    glGetShaderInfoLog(shader, 512, nullptr, buf);
    std::cout << buf << std::endl;
}

int main()
{
    SDLContext ctx{3, 2};

    glewExperimental = true;
    glewInit();

    GLuint vao;
    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);

    float vertices[] = {0.f, 0.5f, 0.5f, -0.5f, -0.5f, -0.5f};
    GLuint vbo;
    glGenBuffers(1, &vbo);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

    GLuint vertexShader = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vertexShader, 1, &vertexSource, nullptr);
    glCompileShader(vertexShader);
    check_shader_compilation_status(vertexShader);

    GLuint fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fragmentShader, 1, &fragmentSource, nullptr);
    glCompileShader(fragmentShader);
    check_shader_compilation_status(fragmentShader);

    GLuint shaderProgram = glCreateProgram();
    glAttachShader(shaderProgram, vertexShader);
    glAttachShader(shaderProgram, fragmentShader);

    glBindFragDataLocation(shaderProgram, 0, "outColor");

    glLinkProgram(shaderProgram);
    glUseProgram(shaderProgram);

    GLint posAttrib = glGetAttribLocation(shaderProgram, "position");
    glVertexAttribPointer(posAttrib, 2, GL_FLOAT, GL_FALSE, 0, 0);
    glEnableVertexAttribArray(posAttrib);

    glDrawArrays(GL_TRIANGLES, 0, 3);

    ctx.handle_events(
        [&ctx](SDL_Event event) { handle_sdl_event(ctx, event); });

    return 0;
}
