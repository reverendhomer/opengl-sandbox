#!/bin/sh

set -eu

infile="$1"
outfile="$2"

echo 'R"glsl(' >"$outfile"
cat "$infile" >>"$outfile"
echo ')glsl"' >>"$outfile"
